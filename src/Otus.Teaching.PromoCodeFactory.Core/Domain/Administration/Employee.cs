﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

        override public bool SetData(BaseEntity entity)
        {
            try
            {
                var emp = entity as Employee;

                this.LastName = emp.LastName;
                this.FirstName = emp.FirstName;                
                this.Email = emp.Email;
                this.AppliedPromocodesCount = emp.AppliedPromocodesCount;

                // ...

                this.Roles.Clear();
                this.Roles.AddRange(emp.Roles);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}