﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }               

        virtual public bool SetData(BaseEntity entity) { return false; }
    }
}